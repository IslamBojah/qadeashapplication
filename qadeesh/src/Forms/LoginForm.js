import React,{Component} from 'react';
import LockIcon from '@material-ui/icons/LockOutlined';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import '../css/Login.css'
import '../css/ResponsiveLogin.css'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '../Component/TextField'
import {login} from '../actions/auth'
import {connect} from "react-redux"


const styles = () => ({
    avatar:{marginTop: '10px',color: '#fff', backgroundColor:'#212121'},
  
  });

  
class LoginForm extends Component{
     state={
         data:{
             email:'',
             password:''
         },
         emailerror:false,
         passworderror:false,

         emailhelperText:'',
         passwordhelperText:'',
     }

    signIn=(e)=>{
        e.preventDefault();
        if(this.state.data.email!=='' && this.state.data.username!==''){
            if(!(this.state.emailerror) && !(this.state.usernameerror))
            {
                this.props.login(this.state.data);            }
        }
        else{
            if( this.state.data.email==='' ){
                this.setState({emailerror:true})
                this.setState({emailhelperText:'required'})
            }
            if( this.state.data.username===''){
                this.setState({usernameerror:true})
                this.setState({usernamehelperText:'required'})
            }          
        }
    }
    changeEmailValue=(e)=>{
        this.setState({
            data: {...this.state.data, email:e.target.value},
        },()=>{
            if(!(/^\S+@\S+\.\S+/).test(this.state.data.email)){
                this.setState({emailerror:true})
                this.setState({emailhelperText:'enter a valid email'})      
            }
            else{
                this.setState({emailerror:false})
                this.setState({emailhelperText:''}) 
            } 
            if(this.state.data.email===''){
                this.setState({emailerror:true})
                this.setState({emailhelperText:'required'})
            }        
        })
    }
    changePasswordValue=(e)=>{
        this.setState({
            data: {...this.state.data, password:e.target.value},
        },()=>{
            if(this.state.data.password.length<6 ){
                this.setState({passworderror:true})
                this.setState({passwordhelperText:'enter a valid password'})
            }
            else{
                this.setState({passworderror:false})
                this.setState({passwordhelperText:''}) 
            } 
            if(this.state.data.password===''){
                this.setState({passworderror:true})
                this.setState({passwordhelperText:'required'})
            }
        })        
    }

    render(){
        const {classes}=this.props;    
    return(
        <div className="login-module" >
              <Avatar className={classes.avatar}>
              <LockIcon /></Avatar>
              <form className="formlogin">

                <TextField id='email' label=' الايميل*' error={this.state.emailerror} onChange={this.changeEmailValue} value={this.state.data.email} helperText={this.state.emailhelperText} type="search" />
                <TextField id='password' label='كلمة السر*' error={this.state.passworderror} onChange={this.changePasswordValue} value={this.state.data.password} helperText={this.state.passwordhelperText} type="password"/>
                <Button type="submit" fullWidth variant="contained" color="default" className={classes.avatar} 
                onClick={this.signIn}
                >Sign in</Button>
              </form>              
        </div>
    );
    }
}

LoginForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  const maptoProps=(state)=>{
    return{
        login:state.login.allData
    }
  }
  const mapDispatch=dispatch=>{
      return{
        login:(data)=>dispatch(login(data))
      }
  }


export default connect(maptoProps,mapDispatch) (withStyles(styles)(LoginForm));
import React ,{Component}from 'react';
import Avatar from '@material-ui/core/Avatar';
import '../css/NewQuestion.css'
import '../css/ResponsiveLogin.css'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import img from '../images/1.jpg'
import InputLabel from '@material-ui/core/InputLabel';
import ImagesUploader from 'react-images-uploader';
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';


const styles = () => ({
    Avatar:{
        flex:1,
        margin: 10,
        width: 50,
        height: 50,
    },
    textarea:{
        flex:8,
        border:'none',
        borderStyle: 'none', 
        borderColor: 'Transparent', 
        overflow:'hidden',
        outline: 'none',
        resize: 'none',
        scrollBehavior:'smooth',
        fontSize: '24px',
        fontFamily: 'inherit',
        fontWeight:'300',
        letterSpacing: 0,
        lineHeight: '28px',
    },
    inputLabel:{
        color:'#5e5e5e',
        fontFamily: 'Helvetica, Arial, sans-serif',
        fontSize: '20px',
        marginTop: '5px',
        marginRight: '10px',
    },
    card:{
        minWidth: '100px',
        height:'100px',
        backgroundColor:'#212121',
        margin:'10px'
    },
     
    
  });



class NewQuestionForm extends Component{
     
   QuestionChange=(e)=>{
    this.setState({question:e.target.value},()=>{
        //console.log(this.state.question)
        if(this.state.question ==='قدي' || this.state.question ===''){
            this.setState({question:'قديش'});
        }
    });   
   
  }


    state = { 
        question:'قديش',
        images:'',
        file:'',
        isFirstload:"أضف صورة",
        styles: {
            
            loadContainer:
                {width:'80px',
                height:'30px',
                fontSize:'15px',
                fontWeight:'200',
                border:'none',
                borderWidth: 0,
                backgroundColor:'#05A',
                
                },
        pseudobutton:{
                        backgroundColor:'black',
                        border:'none',
                        borderWidth: 0,
                        
                    },
        pseudobuttonContent:{
                    border:'none',
                    borderWidth: 0,
                    borderStyle: 'none',
                    fontSize:'17px',
                    fontFamily:'inherit',
                },
                filesInputContainer:{
                    border:'none',
                    borderWidth: 0,
                    borderStyle: 'none',
                }
            }
        }
  


  handerlAddImage = (e)=> {
    e.preventDefault();  
    console.log(URL.createObjectURL(e.target.files[0]))
    const test=[URL.createObjectURL(e.target.files[0])];
   // console.log(e.target.files[0].length)
    this.setState({file: URL.createObjectURL(e.target.files[0])})
    setTimeout(function(){
        this.setState((prevState) => ({
            images: [...prevState.images,test ],
        }));
        console.log(this.state.images)

    }.bind(this),200)
   
}


    render(){
  
        const {classes}=this.props;
    return(
        <div className="newQuestionPopUp" >
            <div className="newQuestionToolBar">
            <InputLabel htmlFor="component-simple" className={classes.inputLabel}>اسأل قديش</InputLabel>
            </div>
            <div className="newQuestionHeader">
                <Avatar alt="Remy Sharp" src={img} className={classes.Avatar} />
                <textarea value={this.state.question} id="question" className={classes.textarea} onChange={this.QuestionChange} >
                </textarea>
           </div>
           <div className="newQuestionbody"> 
          


        <ImagesUploader
                url="http://localhost:8080/multiple"
                onLoadEnd={(err,) => {
                    if (err) {
                        console.error(err);  
                    }
                   const styles={loadContainer:{backgroundColor:'white'},
                    pseudobutton:{backgroundColor:'black'},
                    }
                   this.setState({styles:styles})
                   this.setState({isFirstload:false})
                }}
                
                styles= {this.state.styles}
                max={3}
                plusElement={this.state.isFirstload}
                   
        />
                 
               
           </div>
              
        </div>
    );
    }
}

NewQuestionForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(NewQuestionForm);
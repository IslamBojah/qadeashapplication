import React,{Component} from 'react';
import Button from '@material-ui/core/Button';
import '../css/Login.css'
import '../css/ResponsiveLogin.css'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '../Component/TextField'
import {isActiveEmail, signup} from '../actions/auth'
import {connect} from "react-redux"

const styles = () => ({
    avatar:{marginTop: '10px',color: '#fff', backgroundColor:'#212121'},
  });

class SignupForm extends Component{
    WAIT_INTERVAL = 1000
    ENTER_KEY = 13
    timer=null
    state={
        data:{
            email:'',
            username:'',
            phone:'',
            password:'',
            passwordConfirm:'',
        },
        emailerror:false,
        usernameerror:false,
        phoneerror:false,
        passworderror:false,
        passwoConfirmerror:false,

        emailhelperText:'',
        usernamehelperText:'',
        phonehelperText:'',
        passwordhelperText:'',
        passwoConfirmhelperText:'',

    }

    triggerChange = () => {
       this.props.isActiveEmail(this.state.data.email)
       setTimeout(function() { 
           if(this.props.isActive){
            this.setState({emailerror:true})
            this.setState({emailhelperText:'this is a used email'})  
           }
           
           // alert(this.props.isActive)

    }.bind(this), 200)
       
    }
    changeEmailValue=(e)=>{
        clearTimeout(this.timer)
        this.setState({
            data: {...this.state.data, email:e.target.value},
        },()=>{
            if(!(/^\S+@\S+\.\S+/).test(this.state.data.email)){
                this.setState({emailerror:true})
                this.setState({emailhelperText:'enter a valid email'})      
            }
            else{
                this.setState({emailerror:false})
                this.setState({emailhelperText:''}) 
                setTimeout(function() { 
                    this.timer = setTimeout(this.triggerChange, this.WAIT_INTERVAL)             
                }.bind(this), 200)
            } 
            if(this.state.data.email===''){
                this.setState({emailerror:true})
                this.setState({emailhelperText:'required'})
            }        
        })

    }
    changeUserNameValue=(e)=>{
        this.setState({
            data: {...this.state.data, username:e.target.value},
        },()=>{
            if(!(/^[\d\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\d\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.data.username) || (/^[\d ]*$/).test(this.state.data.username)){
                this.setState({usernameerror:true})
                this.setState({usernamehelperText:'enter a valid username'})
            }
            else{
                this.setState({usernameerror:false})
                this.setState({usernamehelperText:''}) 
            } 
            if(this.state.data.username===''){
                this.setState({usernameerror:true})
                this.setState({usernamehelperText:'required'})
            }         
        })
    }
    changePhoneValue=(e)=>{

        this.setState({
            data: {...this.state.data, phone:e.target.value},
        },()=>{
          //  alert(this.state.data.phone)
            if(!(/\(?([0]{1})\)?[-. ]?([0-9]{9,})/).test(this.state.data.phone)){
                this.setState({phoneerror:true})
                this.setState({phonehelperText:'enter a valid phone'})
            }
            else{
                this.setState({phoneerror:false})
                this.setState({phonehelperText:''}) 
            } 
            if(this.state.data.phone===''){
                this.setState({phoneerror:true})
                this.setState({phonehelperText:'required'})
            }            
        })
    }
    changePasswordValue=(e)=>{
        this.setState({
            data: {...this.state.data, password:e.target.value},
        },()=>{
            if(this.state.data.password.length<6 ){
                this.setState({passworderror:true})
                this.setState({passwordhelperText:'enter a valid password'})
            }
            else{
                this.setState({passworderror:false})
                this.setState({passwordhelperText:''}) 
            } 
            if(this.state.data.password===''){
                this.setState({passworderror:true})
                this.setState({passwordhelperText:'required'})
            }
                      
        })
    }
    changePasswordConfirmValue=(e)=>{
        this.setState({
            data: {...this.state.data, passwordConfirm:e.target.value},
        },()=>{
            if(this.state.data.password !== this.state.data.passwordConfirm  ){
                this.setState({passworderror:true})
                this.setState({passwordhelperText:'enter a valid password'})
                this.setState({passwordConfirmerror:true})
                this.setState({passwordConfirmhelperText:'enter a valid password'})
            }
            else{
                this.setState({passworderror:false})
                this.setState({passwordhelperText:''})   
                this.setState({passwordConfirmerror:false})
                this.setState({passwordConfirmhelperText:''})
            }            
        }) 
    }
    signup=(e)=>{
       e.preventDefault();
        if(this.state.data.email!=='' && this.state.data.username!==''&& this.state.data.phone!==''&& this.state.data.password!=='' && this.state.data.passwordConfirm!==''){
            if(!(this.state.emailerror) && !(this.state.usernameerror) && !(this.state.phoneerror) && !(this.state.passworderror) && !(this.state.passwordConfirmerror) )
            {
                this.props.signup(this.state.data)
                this.location.reload();
            }
        }
        else{
            if( this.state.data.email==='' ){
                this.setState({emailerror:true})
                this.setState({emailhelperText:'required'})
            }
            if( this.state.data.username===''){
                this.setState({usernameerror:true})
                this.setState({usernamehelperText:'required'})
            }
            if( this.state.data.phone===''){
                this.setState({phoneerror:true})
                this.setState({phonehelperText:'required'})
            }
            if(this.state.data.password===''){
                this.setState({passworderror:true})
                this.setState({passwordhelperText:'required'})
            }
            if(this.state.data.passwordConfirm===''){
                this.setState({passwordConfirmerror:true})
                this.setState({passwordConfirmhelperText:'required'})
            }
          
        }
    }
    render(){
        const {classes}=this.props;    
    return(
        <div className="login-module" >
              <form className="formlogin">
                <TextField id='email' label=' الايميل*' error={this.state.emailerror} onChange={this.changeEmailValue} value={this.state.data.email} helperText={this.state.emailhelperText} type="search" onKeyDown={this.triggerChange}/>
                <TextField id='username' label='اسم المستخدم*' error={this.state.usernameerror} onChange={this.changeUserNameValue} value={this.state.data.username} helperText={this.state.usernamehelperText} type="search"/>
                <TextField id='phone' label=' رقم الهاتف*' error={this.state.phoneerror} onChange={this.changePhoneValue} value={this.state.data.phone} helperText={this.state.phonehelperText} type="search"/>
                <TextField id='password' label='كلمة السر*' error={this.state.passworderror} onChange={this.changePasswordValue} value={this.state.data.password} helperText={this.state.passwordhelperText} type="password"/>
                <TextField id='passwoConfirm' label='تأكيد  كلمة السر*' error={this.state.passwordConfirmerror} onChange={this.changePasswordConfirmValue} value={this.state.data.passwordConfirm} helperText={this.state.passwoConfirmhelperText} type="password"/>
                <Button type="submit" fullWidth variant="contained" color="default" className={classes.avatar} onClick={this.signup} >Sign up</Button>
              </form>
              
        </div>
    );

    }
}

SignupForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };

const maptoProps=(state)=>{
    return{
        isActive:state.login.isActive
    }
}
const mapDispatch=dispatch=>{
    return{
        signup:(data)=>dispatch(signup(data)),
        isActiveEmail:(data)=>dispatch(isActiveEmail(data))
    }
}

  export default connect(maptoProps,mapDispatch)(withStyles(styles)(SignupForm));
import React ,{ Component }from 'react';
import Typography from '@material-ui/core/Typography';
import '../css/Login.css'
import '../css/ResponsiveLogin.css'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import NoSsr from '@material-ui/core/NoSsr';
import Tab from '@material-ui/core/Tab';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';
import { withStyles } from '@material-ui/core/styles';


function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};
const styles=(theme)=>({
  root:{
    backgroundColor: '#212121', 
    color:'#fff',
    fontWeight: 'bold',
    fontSize: '0.875rem',
    
  },
  indicator:{
    backgroundColor:'#4527a0'
  },

 
})

function LinkTab(props) {
  return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

class LogpopupForm extends Component{
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

    render(){
      const { value } = this.state;
      const {classes}=this.props;

      return(
      <NoSsr>
        <div className='Login_tab'>
          <AppBar position="static" >
            <Tabs fullWidth value={value} onChange={this.handleChange}  classes={{root:classes.root ,indicator:classes.indicator}} >
              <LinkTab label="تسجيل الدخول" href="page1" style={{ fontSize: '20px' }} />
              <LinkTab label="حساب جديد" href="page2" style={{ fontSize: '20px' }}/>
            </Tabs>
          </AppBar>
          {value === 0 && <TabContainer>
            <LoginForm></LoginForm>
          </TabContainer>}

          {value === 1 && <TabContainer>
            <SignupForm></SignupForm>
          </TabContainer>}

          </div>
      </NoSsr>
       
       
    );

    }
       
}

LogpopupForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LogpopupForm);


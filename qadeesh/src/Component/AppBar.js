
import React,{Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import '../css/ToolApp.css'
import '../css/ResponsiveFormRes.css'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SearchBar from './SearchBar'
import LoginPage from '../Pages/LoginPage'
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';

import DrawerBar from './Drawer'

const drawerWidth = 240;

class AppBars extends Component{
    state={
        open:false,
        openLogin: false,
    }
    handleDrawerOpen = () => {
        this.setState({ open: true });
      };
    
      handleDrawerClose = (value) => {
          console.log(value);
        this.setState({ open: false });
      };
    
      handleOpen = () => {
        this.setState({ openLogin: true });
      };
      handleClose =() =>{
        this.setState({openLogin:false});
      }
    render(){
    const { classes} = this.props;
    const { open } = this.state;
  
    return(
        <div className={classes.root} >
      
      <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
        
          <div className="leftSide">
     
          {this.state.openLogin && <LoginPage open={this.state.openLogin} close={this.handleClose}></LoginPage> }
             <Button color="primary" className={classes.button} onClick={this.handleOpen}>
            تسجيل الدخول  
            </Button>
            </div>
            <div className="middleBar">
            <SearchBar></SearchBar>
            </div>
            <div className={classes.RightSide}>
            <div className="RightSide">
            <Typography variant="h4" color="inherit" className={classes.grow}>
              قديش
            </Typography>
            </div>
            <IconButton className={classNames(classes.menuButton, open && classes.hide)} color="inherit" aria-label="Menu" onClick={this.handleDrawerOpen} >
              <MenuIcon />
            </IconButton>
            </div>
        </AppBar>
        <DrawerBar openDrawer={open} closeDrawer={this.handleDrawerClose}></DrawerBar>
         
         
        </div>
    )
}
}
const styles =theme=>({
    root:{
        display: 'flex',
      },
    
      RightSide:{
      display:'flex',
      flex:.5,
      height:50,
      justifyContent:'flex-end',
      alignItems:'center',
      
      },
      middle:{
        display:'flex',
        flex:2,
        justifyContent:'center',
      },
      menuButton: {
        marginRight: 8,
        
      },
      grow: {
        marginRight:10,
        marginTop:5,
      },
      
    
      button: {
        margin: theme.spacing.unit,
        fontSize:17,
        color:'white',
        marginRight:-14,
        fontWeight:'bold'
      },
      appBar: {
        display:'flex',
        flexDirection:'row',
        backgroundColor:'#202124',
        transition: theme.transitions.create(['margin', 'width'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
      },
      appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginRight: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },

    });

SearchBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(AppBars);
  
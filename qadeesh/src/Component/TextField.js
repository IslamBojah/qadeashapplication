import React from 'react';
import '../css/Login.css'
import '../css/ResponsiveLogin.css'
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';


const styles =()=>({
      TextField:{    
              width: '100%',
              direction:'rtl',                  
          },
          labeel:{
            backgroundColor:'black',

          },
          cssLabel: {
    
              '&$cssFocused': {
                color: '#4527a0',
                              
              },
            },
            cssFocused: {},
            cssUnderline: {
              '&:after': {
                borderBottomColor: '#4527a0',
              
              },
            },
            cssOutlinedInput: {
              '&$cssFocused $notchedOutline': {
                borderColor: '#4527a0',
               
              },
            },
            notchedOutline: {},
  
  })
const TextFeild= (props)=>{
    const {classes}=props
    return(

        <TextField 
              margin='normal'
              id={props.id} 
              label={props.label} 
              type={props.type} 
              className={classes.TextField} 
              onChange={props.onChange}
              InputLabelProps={{
                classes: {
                  label:classes.labeel,
                  root: classes.cssLabel,
                  focused: classes.cssFocused,
                },
              }}              
              InputProps={{
                classes: {
                  root: classes.cssOutlinedInput,
                  focused: classes.cssFocused,
                  notchedOutline: classes.notchedOutline,
                  underline: classes.cssUnderline,
                },
                
              }}            
              error={props.error}
              helperText={props.helperText}
              />
    
          

    );

}


TextFeild.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(TextFeild);

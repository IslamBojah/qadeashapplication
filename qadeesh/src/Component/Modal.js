import React from 'react';
import Modal from '@material-ui/core/Modal';
import '../css/ToolApp.css'

const modal =(props) =>{
   // console.log(props.open)
    return(
        <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.open}
        onClose={()=> props.close()}
        >
            {props.inside}             
        </Modal>
    );
}
export default modal

import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

const drawerWidth = 240;
const DrawerBar=(props)=>{
const {classes,theme,openDrawer,closeDrawer}=props;
    return (
        <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={openDrawer}
        classes={{ paper: classes.drawerPaper,}}
      >
    
      <div className={classes.drawerHeader}>
      <IconButton onClick={()=>{closeDrawer()}}>
        {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
      </IconButton>
    </div>
    <List>
      {['الاصناف', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
         
        <ListItem button key={text} onClick={()=>alert('welcome')}>
          <ListItemText primary={<Typography variant="h7" style={{ color: '#000',fontSize:15,fontWeight:'normal',marginRight:20}}>{text}</Typography>}   /> 
        </ListItem>
      
      ))}
    </List>
      </Drawer>
    )
}

const styles =theme=>({
    
      hide: {
        display: 'none',
      },
      drawer:{
        width: drawerWidth,
        flexShrink: 0,
       
      },
      drawerPaper: {
        width: drawerWidth,
        backgroundColor: theme.palette.background.paper,
         display:'flex',
         flex:1,
         flexDirection:'column',
         justifyContent:'flex-start',
         alignItems:'flex-end',
         
      },
      drawerHeader: {
        alignItems: 'flex-start',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
        flexDirection:'row',
        display:'flex'
      },
   
    });
    Drawer.propTypes = {
        classes: PropTypes.object.isRequired,
        theme: PropTypes.object.isRequired,
      };
      
      export default withStyles(styles,{ withTheme: true })(DrawerBar);

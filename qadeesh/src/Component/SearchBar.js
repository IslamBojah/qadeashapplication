
import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import PropTypes from 'prop-types';

const styles=theme=>({

    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'fix',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      search: {
        position: 'fix  ',
        direction:'rtl',
        display: 'flex',  
        alignItems: 'center',
        justifyContent: 'flex-end',
        borderRadius: theme.shape.borderRadius,
        width: '80%',
        backgroundColor: fade(theme.palette.common.white, 0.10),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
     
      },
    inputRoot: {
        color: 'inherit',
        width: '100%',
      },
      inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: 200,
        },
      },
})

const SearchBar=(props)=>{
    const { classes } = props;
    return(

              <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
              />
            </div>
    )
}

SearchBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(SearchBar);
  
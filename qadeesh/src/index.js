import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {userlogindata} from './actions/auth'
import {createStore,applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
import rootReducer from './reducer/rootreducer'

const store=createStore(rootReducer,composeWithDevTools(applyMiddleware(thunk)))


if(localStorage.token){
    const user={token:localStorage.token}
    store.dispatch(userlogindata(user))
}

ReactDOM.render(<Provider store={store}><App/></Provider> , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();


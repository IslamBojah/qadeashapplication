import axios from 'axios' 


export default {

    userlogin:{
        userlogin:data=>
            axios.post(('api/login'),{data}).then(res=>res.data.user)
    },
    usersignup:{
        usersignup:data=>
            axios.post(('api/signup'),{data})
    },
    isActiveEmail:{
        isActiveEmail:data=>
            axios.post(('api/isActiveEmail'),{data}).then(res=>res.data.user)
    }
}
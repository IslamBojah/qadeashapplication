import api from './api'
import {LOGIN,SIGNUP,ACTIVEEMAIl} from './types'


export const userlogindata=(data)=>{
    return{
        type:LOGIN,
        data
    }
}
export const usersignupdata=(data)=>{
    return{
        type:SIGNUP,
        data
    }
}
export const ActiveEmaildata=(data)=>{
    return{
        type:ACTIVEEMAIl,
        data
    }
}



export const login=data=>dispatch=>{
    api.userlogin.userlogin(data).then(user=>{
        localStorage.token=user.token;
        localStorage.userType=user.types;
        localStorage.username=user.username;
        dispatch(userlogindata(user))
    })
}
export const logout=()=>dispatch=>{
    localStorage.removeItem("token");
   // dispatch(userLogOUt())   
}
export const signup=data=>dispatch=>{
    api.usersignup.usersignup(data).then(allData=>dispatch(usersignupdata(allData)))
}
export const isActiveEmail=data=>dispatch=>{
    api.isActiveEmail.isActiveEmail(data).then(result=>dispatch(ActiveEmaildata(result)))
}
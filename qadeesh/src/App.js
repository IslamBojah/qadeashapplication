import React, { Component } from 'react';
import './App.css'
import AppBar from './Component/AppBar'
import Button from '@material-ui/core/Button';
import NewQuestionPage from './Pages/NewQuestionPage'
import {login} from './actions/auth'
import {connect} from "react-redux"


class App extends Component {
  state={
    NewQuestion:false,
  }

  openNewQuestion =()=>{
    this.props.login("hello");
   // this.setState({NewQuestion:true});
   setTimeout(function() { //Start the timer
    alert(this.props.login1)

   }.bind(this),500)
  }

  closeNewQuestion =()=>{
    this.setState({NewQuestion:false});
  }

  render() {
    

    return (

        <div className="App">
         <AppBar></AppBar>
         <Button color="primary" 
         style={{marginTop:'100px',backgroundColor:'#ff9100',color:'white'}}
         onClick={this.openNewQuestion}
         >hello world</Button>

         {this.state.NewQuestion && 
         
         <NewQuestionPage open={this.state.NewQuestion} close={this.closeNewQuestion}></NewQuestionPage>
         
         }
       
        </div>
 
    );
}
}

const mapdispatch = dispatch =>{
  return{
    login:(data)=>dispatch(login(data))
  }
}
const maptoProps=(state)=>{
  return{
      login1:state.login.allData
  }
}
export default connect(maptoProps,mapdispatch) (App)

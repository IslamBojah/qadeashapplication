import React from 'react';
import Modal from '../Component/Modal'
import LogpopupForm from '../Forms/LogpopupForm';

const login=<LogpopupForm></LogpopupForm>

const LoginPage =(props)=>{

    return (  
      <div>
        <Modal open={props.open} close={()=>{props.close()}} inside={login}></Modal>
      </div>
        
    );
  
}


export default LoginPage;
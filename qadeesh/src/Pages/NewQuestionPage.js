import React from 'react';
import Modal from '../Component/Modal'
import NewQuestion from '../Forms/NewQuestionForm'

const newQuestion=<NewQuestion></NewQuestion>

const NewQuestionPage =(props)=>{
    console.log(props);
    return (  
      <div>
        <Modal open={props.open} close={()=>{props.close()}} inside={newQuestion}></Modal>
      </div>
        
    );
  
}



export default NewQuestionPage;